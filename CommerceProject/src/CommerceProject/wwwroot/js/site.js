﻿//Messages Singleton
var Messages = new function(){

    this.displaySuccessMsg = function(input) {
                swal({
                    title: "Checked " + input + " successfully",
                    text: "Redirecting to Home...",
                    type: "success",
                    showConfirmButton: false,
                    timer: 700
                },
                function(){
                    window.location.href = '/';
                });
        };

    this.displayErrorMsg = function() {
        swal({
                title: "Unable to Check In Accounts",
                text: "If problem persist contact adminastrator",
                type: "error"
            });
    };

    this.warningMsg = function() {
        swal({
            title: "Did you select any values?",
            type: "warning",
            confirmButtonText: "Ok",
            closeOnConfirm: false
        });
    };

    /* (In case we need it...)
    This will disable the buttons unless something is checked.
    var checkBoxes = $('tbody #selected');
    checkBoxes.change(function () {
        $('#checkinbutton').prop('disabled', checkBoxes.filter(':checked').length < 1);
    });
    $('tbody #selected').change();
    */


};
