﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace CommerceProject.Models
{
    public partial class MRcfCrf
    {
        public MRcfCrf()
        {
            MRcfCac = new HashSet<MRcfCac>();
        }

        public DateTime DataDate { get; set; }
        public string RfcrffRecType { get; set; }
        public int RfcrffInst { get; set; }
        public string RfcrffCustType { get; set; }
        public string RfcrffAlpha { get; set; }
        public int RfcrffAccum { get; set; }
        public int RfcrffTiebrkr { get; set; }
        public string RfcrffStatus { get; set; }
        public string RfcrffName { get; set; }
        public string RfcrffMailCd { get; set; }
        public string RfcrffStAddr { get; set; }
        public string RfcrffAddrSupp { get; set; }
        public string RfcrffAddrExt { get; set; }
        public string RfcrffCity { get; set; }
        public string RfcrffState { get; set; }
        public int? RfcrffZip5 { get; set; }
        public int? RfcrffZip4 { get; set; }
        public string RfcrffPostal { get; set; }
        public string RfcrffProv { get; set; }
        public string RfcrffCountryCd { get; set; }
        public string RfcrffRouteCd { get; set; }
        public string RfcrffDeliveryPt { get; set; }
        public int? RfcrffAddrCkCd { get; set; }
        public decimal? RfcrffHphone { get; set; }
        public decimal? RfcrffBphone { get; set; }
        public int? RfcrffPin1 { get; set; }
        public int? RfcrffPin2 { get; set; }
        public string RfcrffMailing { get; set; }
        public string RfcrffComment1 { get; set; }
        public string RfcrffComment2 { get; set; }
        public string RfcrffTinType { get; set; }
        public string RfcrffTaxId { get; set; }
        public string RfcrffTaxStatus { get; set; }
        public int? RfcrffTaxStatusDt { get; set; }
        public string RfcrffDesc1 { get; set; }
        public string RfcrffDesc2 { get; set; }
        public string RfcrffDesc3 { get; set; }
        public string RfcrffDesc4 { get; set; }
        public string RfcrffDesc5 { get; set; }
        public int? RfcrffBranch { get; set; }
        public string RfcrffOfficer { get; set; }
        public string RfcrffOfficer2 { get; set; }
        public string RfcrffRestrict { get; set; }
        public string RfcrffGlRptCd { get; set; }
        public string RfcrffFstCtac { get; set; }
        public int? RfcrffFstCtacDt { get; set; }
        public string RfcrffLstCtac { get; set; }
        public int? RfcrffLstCtacDt { get; set; }
        public string RfcrffReferralCd { get; set; }
        public string RfcrffSensitiveInd { get; set; }
        public string RfcrffMktgCd { get; set; }
        public int? RfcrffCrRatingDt { get; set; }
        public string RfcrffCrRating { get; set; }
        public string RfcrffCycle { get; set; }
        public string RfcrffRiskCd { get; set; }
        public string RfcrffServiceLvl { get; set; }
        public int? RfcrffMaintType { get; set; }
        public int? RfcrffMaintDt { get; set; }
        public string RfcrffDedupeFlag { get; set; }
        public string RfcrffCommentFlag { get; set; }
        public int? RfcrffDob { get; set; }
        public string RfcrffSex { get; set; }
        public string RfcrffMarital { get; set; }
        public string RfcrffOccupCd { get; set; }
        public int? RfcrffEmplDt { get; set; }
        public string RfcrffEducLvl { get; set; }
        public string RfcrffIncClass { get; set; }
        public string RfcrffOwnRentCd { get; set; }
        public string RfcrffMaidenName { get; set; }
        public string RfcrffCurrEmpl { get; set; }
        public string RfcrffEthnic { get; set; }
        public string RfcrffPriLang { get; set; }
        public string RfcrffCtznCd { get; set; }
        public string RfcrffDnsBod { get; set; }
        public string RfcrffSeniorExec { get; set; }
        public string RfcrffUserCd1Rest { get; set; }
        public string RfcrffUserCd2 { get; set; }
        public string RfcrffUserCd3 { get; set; }
        public string RfcrffUserCd4 { get; set; }
        public string RfcrffUserDemo { get; set; }
        public string RfcrffCensusTr { get; set; }
        public int? RfcrffDeathDt { get; set; }
        public int? RfcrffBphoneExtn { get; set; }
        public string RfcrffPrivacyCd { get; set; }
        public int? RfcrffPrivacyDt { get; set; }
        public string RfcrffElectionCd1 { get; set; }
        public int? RfcrffElectionDt1 { get; set; }
        public string RfcrffElectionCd2 { get; set; }
        public int? RfcrffElectionDt2 { get; set; }
        public string RfcrffElectionCd3 { get; set; }
        public int? RfcrffElectionDt3 { get; set; }
        public int? RfcrffCustNo { get; set; }
        public string RfcrffLegalCd { get; set; }
        public string RfcrffBirthCtry { get; set; }
        public string RfcrffTinCertCd { get; set; }
        public int? RfcrffTinCertDt { get; set; }
        public int? RfcrffLastCipActyDt { get; set; }
        public decimal? RfcrffCphone { get; set; }
        public string RfcrffReserved { get; set; }
        public string RfcrffNamePrefix { get; set; }
        public string RfcrffNameFirst { get; set; }
        public string RfcrffNameMidd1 { get; set; }
        public string RfcrffNameMidd2 { get; set; }
        public string RfcrffNameLast { get; set; }
        public string RfcrffNameSuffix1 { get; set; }
        public string RfcrffNameSuffix2 { get; set; }

        public virtual ICollection<MRcfCac> MRcfCac { get; set; }

        public string getFieldValue(string fieldName)
        {
            Type type = typeof(MRcfCrf);
            MemberInfo[] members = type.GetMember(fieldName, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            if (members.Length > 0)
            {
                var member = members[0];
                if (member is PropertyInfo)
                {
                    var fieldValue = ((PropertyInfo)member).GetValue(this);
                    if (fieldValue != null)
                    {
                        return fieldValue.ToString();
                    }
                    return "";
                }
            }
            return "";
        }

        public bool Equals(MRcfCrf other)
        {
            return this.RfcrffAlpha.Equals(other.RfcrffAlpha);
        }
    }
}
