﻿
using System;
using System.Collections.Generic;

namespace CommerceProject.Models
{
    public partial class LockedAccounts
    {
        public List<MRcfCac> accounts;
        public Dictionary<string, string> accountsAndHolders;
    }
}