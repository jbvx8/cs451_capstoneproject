﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommerceProject.Models
{
    public class CustomerResult
    {
        public Dictionary<MRcfCrf, List<MRcfCac>> customerAccounts { get; set; }
        public string accountsToLock { get; set; }
        public Dictionary<string, LockedAccount> accountLockStatus { get; set; }
        public string[] selected { get; set; }
    }
}
