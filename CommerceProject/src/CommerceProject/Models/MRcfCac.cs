﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel.DataAnnotations;
namespace CommerceProject.Models
{
    public partial class MRcfCac
    {
        public DateTime DataDate { get; set; }
        [Display(Name = "Record Type")]
        public string RfcacfRecType { get; set; }
        public int? RfcacfInst { get; set; }
        public string RfcacfCustType { get; set; }
        public string RfcacfAlpha { get; set; }
        public int? RfcacfAccum { get; set; }
        public int? RfcacfTiebrkr { get; set; }
        public int? RfcacfAcctInst { get; set; }
        public string RfcacfProdcode { get; set; }
        public string RfcacfAcct { get; set; }
        public int? RfcacfPrimFlag { get; set; }
        public int? RfcacfRelNbr { get; set; }

        public virtual MRcfCrf RfcacfAlphaNavigation { get; set; }

        public string getFieldValue(string fieldName)
        {
            Type type = typeof(MRcfCac);
            MemberInfo[] members = type.GetMember(fieldName, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            if (members.Length > 0)
            {
                var member = members[0];
                if (member is PropertyInfo)
                {
                    var fieldValue = ((PropertyInfo)member).GetValue(this);
                    if (fieldValue != null)
                    {
                        return fieldValue.ToString();
                    }
                    return "";
                }
            }
            return "";
        }

        public bool Equals(MRcfCac other)
        {
            return this.RfcacfAcct.Equals(other.RfcacfAcct) && this.RfcacfAlpha.Equals(other.RfcacfAlpha);
        }
    }

}
