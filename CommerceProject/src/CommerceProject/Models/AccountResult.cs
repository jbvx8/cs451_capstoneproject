﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CommerceProject.Models
{
    public class AccountResult
    {
        public List<MRcfCac> accounts;
        public List<MRcfCac> relatedAccounts;
        public string accountsToLock;
        public Dictionary<string, LockedAccount> accountLockStatus;
        public string[] selected;
    }
    
}
