﻿using System;
using System.Collections.Generic;

namespace CommerceProject.Models
{
    public partial class LockedAccount
    {
        //public int Id { get; set; }
        public string AccountNumber { get; set; }
        public string User { get; set; }
        public DateTime? DateTime { get; set; }
        public string Link { get; set; }
    }
}
