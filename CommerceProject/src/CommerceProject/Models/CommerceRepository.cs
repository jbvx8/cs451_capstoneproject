﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using System.Reflection;

namespace CommerceProject.Models
{
    public class CommerceRepository
    {
        public Dictionary<string, string> displayName = new Dictionary<string, string>();

        private CommerceDbContext db;

        public CommerceRepository(CommerceDbContext context)
        {
            db = context;
            displayName.Add("RfcrffRecType", "Record Type");
            displayName.Add("RfcrffInst", "Instance");
            displayName.Add("RfcrffCustType", "Customer Type");
            displayName.Add("RfcrffAlpha", "Name Alpha");
            displayName.Add("RfcrffAccum", "Accum");
            displayName.Add("RfcrffTiebrkr", "Tie Breakers");
            displayName.Add("RfcrffStatus", "Status");
            displayName.Add("RfcrffName", "Name");
            displayName.Add("RfcrffMailCd", "Mail CD");
            displayName.Add("RfcrffStAddr", "Street Address");
            displayName.Add("RfcrffAddrSupp", "Support Address");
            displayName.Add("RfcrffAddrExt", "Address Extension");
            displayName.Add("RfcrffCity", "City");
            displayName.Add("RfcrffState", "State");
            displayName.Add("RfcrffZip5", "Zip(5)");
            displayName.Add("RfcrffZip4", "Zip(4)");
            displayName.Add("RfcrffPostal", "Postal");
            displayName.Add("RfcrffProv", "Provence");
            displayName.Add("RfcrffCountryCd", "Country Code");
            displayName.Add("RfcrffRouteCd", "Route Code");
            displayName.Add("RfcrffDeliveryPt", "Delivery Point");
            displayName.Add("RfcrffAddrCkCd", "AddrCkCD");
            displayName.Add("RfcrffHphone", "Home Phone");
            displayName.Add("RfcrffBphone", "Business Phone");
            displayName.Add("RfcrffPin1", "Pin1");
            displayName.Add("RfcrffPin2", "Pin2");
            displayName.Add("RfcrffMailing", "Mailing Address");
            displayName.Add("RfcrffComment1", "Comment1");
            displayName.Add("RfcrffComment2", "Comment2");
            displayName.Add("RfcrffTinType", "TIN Type");
            displayName.Add("RfcrffTaxId", "Tax ID");
            displayName.Add("RfcrffTaxStatus", "Tax Status");
            displayName.Add("RfcrffTaxStatusDt", "Tax Status Dt");
            displayName.Add("RfcrffDesc1", "Description1");
            displayName.Add("RfcrffDesc2", "Description2");
            displayName.Add("RfcrffDesc3", "Description3");
            displayName.Add("RfcrffDesc4", "Description4");
            displayName.Add("RfcrffDesc5", "Description5");
            displayName.Add("RfcrffBranch", "Bank Branch Number");
            displayName.Add("RfcrffOfficer", "Officer Number");
            displayName.Add("RfcrffOfficer2", "Officer 2 Number");
            displayName.Add("RfcrffRestrict", "Restrict");
            displayName.Add("RfcrffGlRptCd", "GlRptCD");
            displayName.Add("RfcrffFstCtac", "FstCtac");
            displayName.Add("RfcrffFstCtacDt", "FstCtacDt");
            displayName.Add("RfcrffLstCtac", "LstCtac");
            displayName.Add("RfcrffLstCtacDt", "LstCtacDt");
            displayName.Add("RfcrffReferralCd", "ReferralCd");
            displayName.Add("RfcrffSensitiveInd", "SensitiveInd");
            displayName.Add("RfcrffMktgCd", "MktgCd");
            displayName.Add("RfcrffCrRatingDt", "CrRatingDt");
            displayName.Add("RfcrffCrRating", "CrRating");
            displayName.Add("RfcrffCycle", "Cycle");
            displayName.Add("RfcrffRiskCd", "RiskCd");
            displayName.Add("RfcrffServiceLvl", "Service Level");
            displayName.Add("RfcrffMaintType", "Maint Type");
            displayName.Add("RfcrffMaintDt", "Maint Dt");
            displayName.Add("RfcrffDedupeFlag", "Dedupe Flag");
            displayName.Add("RfcrffCommentFlag", "Comment Flag");
            displayName.Add("RfcrffDob", "Date of Birth)");
            displayName.Add("RfcrffSex", "Sex (M/F)");
            displayName.Add("RfcrffMarital", "Marital Status");
            displayName.Add("RfcrffOccupCd", "Occupation Cd");
            displayName.Add("RfcrffEmplDt", "EmployeeIDt");
            displayName.Add("RfcrffEducLvl", "Education Level");
            displayName.Add("RfcrffIncClass", "Income Class");
            displayName.Add("RfcrffOwnRentCd", "OwnRent Cd");
            displayName.Add("RfcrffMaidenName", "Maiden Name");
            displayName.Add("RfcrffCurrEmpl", "Current Employer");
            displayName.Add("RfcrffEthnic", "Ethnicity");
            displayName.Add("RfcrffPriLang", "Primary Language");
            displayName.Add("RfcrffCtznCd", "Citizen Cd");
            displayName.Add("RfcrffDnsBod", "DnsBod");
            displayName.Add("RfcrffSeniorExec", "Senior Executive");
            displayName.Add("RfcrffUserCd1Rest", "User Cd1 Rest");
            displayName.Add("RfcrffUserCd2", "User Cd2");
            displayName.Add("RfcrffUserCd3", "User Cd3");
            displayName.Add("RfcrffUserCd4", "User Cd4");
            displayName.Add("RfcrffUserDemo", "User Demo");
            displayName.Add("RfcrffCensusTr", "Census Tract");
            displayName.Add("RfcrffDeathDt", "Death Date");
            displayName.Add("RfcrffBphoneExtn", "Business Phone ext. Number");
            displayName.Add("RfcrffPrivacyCd", "Privacy Cd.");
            displayName.Add("RfcrffPrivacyDt", "Privacy Dt.");
            displayName.Add("RfcrffElectionCd1", "Election Cd1");
            displayName.Add("RfcrffElectionDt1", "Election Dt1");
            displayName.Add("RfcrffElectionCd2", "Election Cd2");
            displayName.Add("RfcrffElectionDt2", "Election Dt2");
            displayName.Add("RfcrffElectionCd3", "Election Cd3");
            displayName.Add("RfcrffElectionDt3", "Election Dt3");
            displayName.Add("RfcrffCustNo", "Customer Number");
            displayName.Add("RfcrffLegalCd", "Legal Cd.");
            displayName.Add("RfcrffBirthCtry", "Birth Country");
            displayName.Add("RfcrffTinCertCd", "TIN Cert Cd.");
            displayName.Add("RfcrffTinCertDt", "Tin Cert Dt.");
            displayName.Add("RfcrffLastCipActyDt", "LastCipActyDt.");
            displayName.Add("RfcrffCphone", "Cell phone");
            displayName.Add("RfcrffReserved", "Reserved");
            displayName.Add("RfcrffNamePrefix", "Name-Prefix");
            displayName.Add("RfcrffNameFirst", "First Name");
            displayName.Add("RfcrffNameMidd1", "Middle Name 1");
            displayName.Add("RfcrffNameMidd2", "Middle Name 2");
            displayName.Add("RfcrffNameLast", "Last Name");
            displayName.Add("RfcrffNameSuffix1", "Name Suffix 1");
            displayName.Add("RfcrffNameSuffix2", "Name Suffix 2");
            displayName.Add("RfcacfRecType", "Record Type");
            displayName.Add("RfcacfInst", "Instance");
            displayName.Add("RfcacfCustType", "Customer Type");
            displayName.Add("RfcacfAlpha", "Alpha");
            displayName.Add("RfcacfAccum", "Accum");
            displayName.Add("RfcacfTiebrkr", "Tiebreakers");
            displayName.Add("RfcacfAcctInst", "Account Instance");
            displayName.Add("RfcacfProdcode", "Prodcode");
            displayName.Add("RfcacfAcct", "Account");
            displayName.Add("RfcacfPrimFlag", "Primary Flag");
            displayName.Add("RfcacfRelNb", "RelNb");
        }


        // Query Methods
        
        public List<string> getColumnData(string columnName)
        {
            IQueryable<string> values = null;
            if (columnName.StartsWith("Rfcac"))
            {
                values = db.MRcfCac.Select(x => x.GetType().GetProperty(columnName).GetValue(x).ToString());
            } else
            {
                values = db.MRcfCrf.Select(x => x.GetType().GetProperty(columnName).GetValue(x).ToString());
            }
            
            return values.Distinct().ToList();
        }

        public List<MRcfCac> getAccountsByQuery(string query)
        {
            var accounts = from a in db.MRcfCac
                               .Where(query)
                               .Include(a => a.RfcacfAlphaNavigation)
                           select a;
            return accounts.ToList();
        }

        public List<MRcfCac> getAccountsByNumber(string account)
        {
            var accounts = from a in db.MRcfCac
                                .Where(a => a.RfcacfAcct == account)
                                .Include(a => a.RfcacfAlphaNavigation)
                           select a;
            return accounts.ToList();
        }

        public List<MRcfCac> getAccountsByCustomerAlpha(string alpha)
        {
            var accounts = from a in db.MRcfCac
                            .Where(a => a.RfcacfAlpha == alpha)
                            .Include(a => a.RfcacfAlphaNavigation)
                           select a;
            return accounts.ToList();
        }

        public List<MRcfCrf> getCustomersByQuery(string query)
        {
            var customers = from c in db.MRcfCrf
                               .Where(query)
                               .Include(c => c.MRcfCac)
                            select c;
            return customers.ToList();
        }

        public List<MRcfCrf> getCustomersByWildcardSearch(string searchTerm)
        {
            var customers = (from c in db.MRcfCrf
                             where c.RfcrffName.Contains(searchTerm)
                             || c.RfcrffNameFirst.Contains(searchTerm)
                             || c.RfcrffNameLast.Contains(searchTerm)
                             || c.RfcrffMaidenName.Contains(searchTerm)
                             || c.RfcrffAlpha.Contains(searchTerm)
                             select c).ToList();
            return customers;
                            
        } 

        public List<MRcfCrf> getCustomersByAccountNumber(string account)
        {
            var customers = from c in db.MRcfCac
                            .Where(c => c.RfcacfAcct == account)
                            .Include(c => c.RfcacfAlphaNavigation)
                            select c.RfcacfAlphaNavigation;
            return customers.ToList();
        }

        public List<MRcfCrf> getAttributesByQuery(string query)
        {
            var getattribute = from c in db.MRcfCrf
                               .Where(query)
                               .Include(c => c.MRcfCac)
                               select c;
            return getattribute.ToList();
        }

        public List<MRcfCac> getAccountsAttributesByQuery(string acctquery)
        {
            var getattributeaccount = from a in db.MRcfCac
                               .Where(acctquery)
                               .Include(a => a.RfcacfAlphaNavigation)
                                      select a;
            return getattributeaccount.ToList();
        }

        public List<LockedAccount> getLockedAccountsByUser(string username)
        {
            var lockedAccounts = from a in db.LockedAccount
                                 .Where(a => a.User == username)
                                 select a;
            return lockedAccounts.ToList();
        }

        public LockedAccount getLockedAccountByNumber(string account)
        {
            return db.LockedAccount.Find(account);
        }

        public Boolean isAccountLocked(string account)
        {
            var lockedAccount = from a in db.LockedAccount
                                .Where(a => a.AccountNumber == account)
                                select a;
            if (lockedAccount.FirstOrDefault() == null)
            {
                return false;
            }
            return true;
        }

        public Dictionary<string, string> getUsersAndTheHolder()
        {
            Dictionary<string, string> users = new Dictionary<string, string>();
            var usrs = from a in db.LockedAccount
                       select a;
            foreach (var user in usrs)
            {
                users.Add(user.AccountNumber, user.User);
            }
            return users;
        }

        public List<MRcfCac> getMRcfCacList()
        {
            return db.MRcfCac.ToList();
        }

        public async Task getMRcfCrfList()
        {
           await db.MRcfCrf.ToListAsync();
        }
        public List<string> accountAttributes()
        {
            Models.MRcfCac test = new Models.MRcfCac();
            Type myType = typeof(Models.MRcfCac);
            FieldInfo[] myField = myType.GetFields(BindingFlags.Public |
                                              BindingFlags.NonPublic |
                                              BindingFlags.Instance);
            List<string> List = new List<string>();
            for (int i = 1; i < myField.Length-1; i++)  //begin at 1 to skip date attribute
            {
                string S = myField[i].Name;
                string S2 = myField[i].FieldType.Name;
                char delimiter = '<';
                char delimiter2 = '>';
                String[] substrings = S.Split(delimiter);
                substrings = substrings[1].Split(delimiter2);
                S = substrings[0];
                List.Add(S);
                if (S2.Equals("Nullable`1"))
                {
                    S2 = "Int32";
                }
                List.Add(S2);
                string S3 = S;
                if (displayName.ContainsKey(S))
                {
                    S3 = displayName[S];
                }
                List.Add(S3);
            }
            return List;
        }
        public List<string> customerAttributes()
        {
            Models.MRcfCrf test = new Models.MRcfCrf();
            Type myType = typeof(Models.MRcfCrf);
            FieldInfo[] myField = myType.GetFields(BindingFlags.Public |
                                              BindingFlags.NonPublic |
                                              BindingFlags.Instance);
            List<string> List = new List<string>();
            for (int i = 1; i < myField.Length-1; i++) //start at 1 to skip date attribute end at -1 to skip other bad attribute
            {
                string S = myField[i].Name;
                string S2 = myField[i].FieldType.Name;
                
                char delimiter = '<';
                char delimiter2 = '>';
                String[] substrings = S.Split(delimiter);
                substrings = substrings[1].Split(delimiter2);
                S = substrings[0];
                List.Add(S);
                if (S2.Equals("Nullable`1"))
                {
                    S2 = "Int32";
                }
                List.Add(S2);
                string S3 = S;
                if (displayName.ContainsKey(S))
                {
                    S3 = displayName[S];
                }
                List.Add(S3);
            }
            return List;
        }
        public AccountResult searchByAccountNumber(string accountNumber)
        {
            List<string> customersToProcess = new List<string> { };
            List<string> accountsProcessed = new List<string> { };
            List<MRcfCac> relatedAccounts = new List<MRcfCac> { };
            AccountResult accountResult = new AccountResult();
            accountResult.accounts = new List<MRcfCac> { };
            accountResult.accountLockStatus = new Dictionary<string, LockedAccount> { };
            HashSet<string> accountsToLock = new HashSet<string> { };

            // get accounts from the accounts table that match the query
            var accounts = getAccountsByNumber(accountNumber);

            if (accounts == null)
            {
                return null;
            }

            accountsProcessed.Add(accountNumber);
            accountsToLock.Add(accountNumber);

            foreach (var account in accounts)
            {
                if (!customersToProcess.Contains(account.RfcacfAlpha))
                {
                    accountResult.accounts.Add(account);
                    customersToProcess.Add(account.RfcacfAlpha);
                }
            }

            relatedAccounts = getRelatedAccounts(customersToProcess, accountsProcessed, new List<string> { });
            foreach (var relatedAccount in relatedAccounts)
            {
                accountsToLock.Add(relatedAccount.RfcacfAcct);
            }

            if (isAccountLocked(accountNumber))
            {
                accountResult.accountLockStatus.Add(accountNumber, getLockedAccountByNumber(accountNumber));
            }
            else
            {
                accountResult.accountLockStatus.Add(accountNumber, null);
            }
            accountResult.accountsToLock = String.Join<string>(",", accountsToLock);
            accountResult.relatedAccounts = relatedAccounts;
            return accountResult;
        }

        public List<CustomerResult> searchByCustomers(List<MRcfCrf> customers)
        {
            List<CustomerResult> results = new List<CustomerResult> { };
            if (customers == null)
            {
                return results;
            }

            foreach (var customer in customers)
            {
                CustomerResult customerResult = new CustomerResult();
                customerResult.customerAccounts = new Dictionary<MRcfCrf, List<MRcfCac>> { };
                HashSet<string> accountsToLock = new HashSet<string> { };
                customerResult.accountLockStatus = new Dictionary<string, LockedAccount> { };
                List<string> customersToProcess = new List<string> { customer.RfcrffAlpha };
                List<MRcfCac> relatedAccounts = new List<MRcfCac> { };

                var customerAccounts = getAccountsByCustomerAlpha(customer.RfcrffAlpha);

                relatedAccounts = getRelatedAccounts(customersToProcess, new List<string> { }, new List<string> { });
                foreach (var relatedAccount in relatedAccounts)
                {
                    string account = relatedAccount.RfcacfAcct;
                    accountsToLock.Add(account);
                    if (isAccountLocked(account))
                    {
                        customerResult.accountLockStatus.Add(account, getLockedAccountByNumber(account));
                    }
                    else
                    {
                        customerResult.accountLockStatus.Add(account, null);
                    }
                }
                relatedAccounts = relatedAccounts.Except(customerAccounts).ToList();

                customerResult.accountsToLock = String.Join<string>(",", accountsToLock);
                customerResult.customerAccounts.Add(customer, relatedAccounts);
                results.Add(customerResult);
            }
            return results;
        }
        
        
        public List<CustomerResult> searchByCustomerName(string name)
        {
            List<CustomerResult> results = new List<CustomerResult> { };
            List<MRcfCrf> customers = new List<MRcfCrf> { };
            
            customers = getCustomersByWildcardSearch(name);           
            if (customers == null)
            {
                return results;
            }
            results = searchByCustomers(customers);
            return results;
        }

        

        public List<CustomerResult> searchByAttributeAccount(string acctquery)
        {
            List<CustomerResult> results = new List<CustomerResult> { };
            var accounts = getAccountsAttributesByQuery(acctquery);
            HashSet<MRcfCrf> customers = new HashSet<MRcfCrf> { };

            if (accounts == null)
            {
                return results;
            }
            foreach (var account in accounts)
            {
                customers.Add(account.RfcacfAlphaNavigation);
            }
            results = searchByCustomers(customers.ToList());

            return results;
        }


        public List<CustomerResult> searchByCustomerAndAccountAttributes(string query, string acctquery)
        {
            
            List<MRcfCrf> customers = getCustomersByQuery(query);
            var accounts = getAccountsByQuery(acctquery);
            List<MRcfCrf> accountCustomers = new List<MRcfCrf> { };
           
            foreach(var account in accounts)
            {
                accountCustomers.Add(account.RfcacfAlphaNavigation);
            }
            List<MRcfCrf> totalCustomers = customers.Intersect(accountCustomers).ToList();
            List<CustomerResult> results = searchByCustomers(totalCustomers);
            
            return results;
        }
       

        public List<MRcfCac> getRelatedAccounts(List<string> names, List<string> accountsProcessed, List<string> namesProcessed)
        {
            List<MRcfCac> relatedAccounts = new List<MRcfCac> { };
            
            foreach (string name in names)
            {
                List<string> accountsToProcess = new List<string> { };
                List<string> namesToProcess = new List<string> { };
                namesProcessed.Add(name);
                var accounts = getAccountsByCustomerAlpha(name);
                foreach (var account in accounts)
                {
                    if (!accountsProcessed.Contains(account.RfcacfAcct))
                    {
                        relatedAccounts.Add(account);
                        accountsToProcess.Add(account.RfcacfAcct);
                    }
                }
                foreach (var accountToProcess in accountsToProcess)
                {
                    accountsProcessed.Add(accountToProcess);
                    var customers = getCustomersByAccountNumber(accountToProcess);
                    foreach (var customer in customers)
                    {
                        if (!namesProcessed.Contains(customer.RfcrffAlpha))
                        {
                            var otherAccounts = getAccountsByCustomerAlpha(customer.RfcrffAlpha);
                            foreach(var acct in otherAccounts)
                            {
                                if (!accountsProcessed.Contains(acct.RfcacfAcct))
                                {
                                    relatedAccounts.Add(acct);
                                }
                            }
                        }
                    }
                }
            }
            return relatedAccounts;
        }
        







        // Insert Methods

        public void Add(LockedAccount account)
        {
            db.LockedAccount.Add(account);
        }

        public void checkoutAccounts(string textAccounts, string user, string date, string linkId)
        {
            var accounts = textAccounts.Split(',');
            foreach (var account in accounts)
            {
                if (account.Length < 18)
                {
                    break;
                }
                else
                {
                    //string substr = account.Substring(0, 18);
                    if (!isAccountLocked(account))
                    {
                        LockedAccount lockAccount = new LockedAccount
                        {
                            AccountNumber = account,
                            User = user,
                            DateTime = Convert.ToDateTime(date),
                            Link = linkId
                        };
                        Add(lockAccount);
                        Save();
                    }
                }
            }
        }

        // Delete Methods

        public void Delete(LockedAccount account)
        {
            db.LockedAccount.Remove(account);
        }

        // Save

        public void Save()
        {
           db.SaveChanges();
        }


        //Helper method
        public Boolean isAccountNumberValid(string input)
        {
            int accountDigits = 18;  //Account number has only 18 digits
            return input.All(char.IsDigit) && (input.Length == accountDigits);
        }

    }
}
