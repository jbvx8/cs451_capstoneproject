﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CommerceProject.Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace CommerceProject.Controllers
{
    public class AutocompleteController : Controller
    {

        private CommerceRepository repo;
      

        public AutocompleteController(CommerceDbContext context)
        {
            repo = new CommerceRepository(context);
        }
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Index(string columnName, string prefix)
        {
            List<string> columnData = repo.getColumnData(columnName);
            var options = (from n in columnData where n.StartsWith(prefix, StringComparison.OrdinalIgnoreCase) select n);
            return Json(options);
        }
    }
}
