﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CommerceProject.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using Microsoft.Extensions.Logging;


// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace CommerceProject.Controllers
{
    public class CustomerController : Controller
    {
        private CommerceRepository repo;
        //private CommerceDbContext _context;
        private readonly ILogger<CustomerController> _logger;

        public CustomerController(CommerceDbContext context, ILogger<CustomerController> logger)
        {
            repo = new CommerceRepository(context);
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult SearchByCustomer()
        {
            return View();
        }


        [HttpPost]
        public IActionResult SearchResults(string input)
        {

            List<CustomerResult> results = repo.searchByCustomerName(input);           
            
            
            return View(results);
           
        }       
    }
}

