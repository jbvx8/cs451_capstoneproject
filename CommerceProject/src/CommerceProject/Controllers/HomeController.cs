﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CommerceProject.Models;
using Microsoft.EntityFrameworkCore;

namespace CommerceProject.Controllers
{
    public class HomeController : Controller
    {
        private CommerceRepository repo;

        public HomeController(CommerceDbContext context)
        {
            repo = new CommerceRepository(context);
        }

        public IActionResult Index()
        {
            List<LockedAccount> lockedAccounts = repo.getLockedAccountsByUser(User.Identity.Name);
            Dictionary<string, LinkedAccount> linkedAccounts = new Dictionary<string, LinkedAccount> { };

            List<LockedAccountResult> lockedAccountResults = new List<LockedAccountResult> { };
            foreach (var account in lockedAccounts)
            {
                List<string> names = new List<string> { };
                var accountResult = repo.getAccountsByNumber(account.AccountNumber);
                foreach (var acct in accountResult)
                {
                    names.Add(acct.RfcacfAlphaNavigation.RfcrffName);
                }
                string name = String.Join<string>(", ", names);
                string accountType = accountResult.FirstOrDefault().RfcacfProdcode;
                string customerType = accountResult.FirstOrDefault().RfcacfCustType;
                LockedAccountResult result = new LockedAccountResult(name, accountType, customerType, account);

                string accountsToCheckIn = null;
                if (linkedAccounts.Any() && linkedAccounts.ContainsKey(account.Link))
                {
                    linkedAccounts[account.Link].linkedLockedAccounts.Add(result);
                    linkedAccounts[account.Link].accountsToCheckIn += "," + result.lockedAccount.AccountNumber;
                }
                else
                {
                    accountsToCheckIn = account.AccountNumber;
                    List < LockedAccountResult > locked = new List<LockedAccountResult> { result };
                    LinkedAccount link = new LinkedAccount(locked, accountsToCheckIn);
                    linkedAccounts.Add(account.Link, link);
                }

                // lockedAccountResults.Add(result);               
            }
           
            return View(linkedAccounts);
        }

        public IActionResult CheckIn(LinkedAccount account)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    foreach (string lockedAccount in account.selected)
                    {
                        if (lockedAccount.Length < 18)
                        {
                            break;
                        } 
                        else
                        {
                            var accounts = lockedAccount.Split(',');
                            foreach (var acct in accounts)
                            {
                                LockedAccount locked = repo.getLockedAccountByNumber(acct);
                                repo.Delete(locked);
                                repo.Save();
                            }
                        }
                    }
                    return Json(new {success = true});
                }
            }
            catch (DbUpdateException ex)
            {
                return Json(new {success = false});
            }
            return Json(new {success = true});
        }
    }
}

