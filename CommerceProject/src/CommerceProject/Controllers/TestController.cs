﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommerceProject.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;

namespace CommerceProject.Controllers
{
    public class TestController : Controller
    {
        private CommerceDbContext _context;

        public TestController(CommerceDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _context.MRcfCac.ToListAsync());
        }

        //public async Task<IActionResult> Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var account = await _context.MRcfCac.Include(a => a.)
        //}

        //public async Task<IActionResult> CustomerResults (String name)
        //{
        //    if (name == null)
        //    {
        //        return NotFound();
        //    }
        //    var customer = await _context.MRcfCrf
        //        .Include(c => c.MRcfCac)
        //        .AsNoTracking()
        //        .SingleOrDefaultAsync(n => n.RfcrffName == name || n.RfcrffAlpha == name || n.RfcrffNameLast == name);

        //    if (customer == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(customer);
        //}

        //public async Task<IActionResult> AccountResults (String account)
        //{
        //    if (account == null)
        //    {
        //        return NotFound();
        //    }
        //    var acct = await _context.MRcfCac
        //        .Include(a => a.RfcacfAlphaNavigation.RfcrffName)
        //        .AsNoTracking()
        //        .SingleOrDefaultAsync(n => n.RfcacfAcct == account);
        //    if (account == null)
        //    {
        //        return NotFound();
        //    }
        //    return View(acct);
        //}

        public IActionResult SearchResults(string type, string[] attributes, string[] ops, string[] args)
        {
            int index = 0;
            int count = attributes.Count();
            string query = null;

            while (index < count)
            {
                query += attributes[index] + " " + ops[index] + " \"" + args[index] + "\"";
                if (index < count - 1)
                {
                    query += " && ";
                }
                index++;
            }

            if (type == "account")
            {
                //string accountQuery = "SELECT * FROM MRcfCac WHERE " + query;
                //var accounts = _context.MRcfCac
                //    .FromSql(accountQuery);

                var accounts = from c in _context.MRcfCac
                               .Where(query)
                               .Include(c => c.RfcacfAlphaNavigation)
                               select c;
                if (accounts == null)
                {
                    return NotFound();
                }
                return View(accounts);
            }
            return View();

        }


        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(MRcfCac account)
        {
            if (ModelState.IsValid)
            {
                _context.MRcfCac.Add(account);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(account);
        }
    }
}
