﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommerceProject.Models;
using Microsoft.AspNetCore.Mvc;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using System.Collections.Immutable;


// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace CommerceProject.Controllers
{

    //Account Handling done in this class. I decided to add basic CRUD operations here in case we need it. 
    //Endpoints would be something like: 
    //   localhost/Account/Search
    //   localhost/Account/Create
    //   localhost/Account/Edit
    //   localhost/Account/Delete
    //   localhost/Account/All  (maybe redirected to table where all customer are visible?)

    // Hmm, I'm thinking the db queries should be moved out of the controller and to the model.
    // TODO break down SearchResults method into more methods
    public class AccountController : Controller
    {
        private CommerceRepository repo;
        //private CommerceDbContext db;
        //private CommerceDbContext _context;

        public AccountController(CommerceDbContext context)
        {
            repo = new CommerceRepository(context);
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult SearchByAccount()
        {
            return View();
        }

        public IActionResult SearchByAttributes()
        {
            List<string> CustomerAttributes = repo.customerAttributes();
            List<string> AccountAttributes = repo.accountAttributes();
            ViewBag.CustomerAttributes = CustomerAttributes;
            ViewBag.AccountAttributes = AccountAttributes;
            return View();
        }

        /*
                //Hardcoded username and password
                public IActionResult Login(string username = null, string password = null)
                {
                    if (username == "admin" && password == "password")
                    {
                        ViewBag.ValidPassword = "true";
                        return View("Search");
                    }
                    if (password != null)
                    {
                        ViewBag.WrongPassword = "true";
                    }
                    return View();
                }
        */

        public IActionResult LockedAccounts()
        {
            List<string> customersToProcess = new List<string> { };
            List<string> accountsProcessed = new List<string> { };
            LockedAccounts lockedAccounts = new LockedAccounts();
            lockedAccounts.accounts = new List<MRcfCac> { };
            var accounts = repo.getMRcfCacList();
            var Lockedaccounts = new List<MRcfCac> { };

            foreach (var account in accounts)
            {
                // add the customer alpha identifier for each account returned to the customersToProcess list
                if (repo.isAccountLocked(account.RfcacfAcct))
                {
                    Lockedaccounts.Add(account);
                }
            }

            lockedAccounts.accountsAndHolders = repo.getUsersAndTheHolder();

            lockedAccounts.accounts = Lockedaccounts;

            return View(lockedAccounts);
        }

        public IActionResult SearchResults(string input)
        {
            if (input == null) 
            {
                return View();
            }
            AccountResult accountResult = repo.searchByAccountNumber(input);
            return View(accountResult);
        }



        public IActionResult SearchAttributesResults(string[] attributes, string[] ops, string[] args)
        {
            int index = 0;
            string query = null;
            string acctquery = null;
            List<string> attributescol = new List<string>();
            List<string> attributesdisplay = new List<string>();
            List<string> attributesTypes = new List<string>();

            Dictionary<MRcfCrf, List<MRcfCac>> result = new Dictionary<MRcfCrf, List<MRcfCac>> { };
            int count = attributes.Count();
            //was index < count
            while (index < count)
            {
                // build search query from attribute names, operators, and user-defined arguments
                if (attributes[index].StartsWith("Rfcrff") && args[index] != null)
                {
                    query += attributes[index] + " " + ops[index] + " \"" + args[index] + "\"";
                    attributesTypes.Add("customer");
                    if (!attributescol.Contains(attributes[index]))
                    {
                        attributescol.Add(attributes[index]);
                        attributesdisplay.Add(repo.displayName[attributes[index]]);
                    }
                    //Console.Write(attributes[index].ToString());
                    //need to take out index < count -1
                    if (index < count - 1 && ((ops[index] == ">" && ops[index + 1] == "<" && attributes[index] == attributes[index + 1]) || (ops[index] == "<" && ops[index + 1] == ">" && attributes[index] == attributes[index + 1]))&& args[index+1] !=null)
                    {
                        query += " && ";
                    }
                    else if (index < count - 1 && attributes[index] != attributes[index + 1] && attributes[index + 1].StartsWith("Rfcrff")&& args[index + 1] != null)
                    {
                        // add and/or selector for attribute search
                        query += " && ";
                    }
                    else if (index < count - 1 && attributes[index + 1].StartsWith("Rfcrff") && args[index + 1] != null)
                    {
                        query += " || ";
                    }
                }
                else
                {
                    if (args[index] != null)
                    {
                        acctquery += attributes[index] + " " + ops[index] + " \"" + args[index] + "\"";
                        attributescol.Add(attributes[index]);
                        attributesdisplay.Add(repo.displayName[attributes[index]]);
                        attributesTypes.Add("account");
                        //Console.Write(attributes[index].ToString());
                        //need to take out index < count -1
                        if (index < count - 1 && ((ops[index] == ">" && ops[index + 1] == "<" && attributes[index] == attributes[index + 1]) || (ops[index] == "<" && ops[index + 1] == ">" && attributes[index] == attributes[index + 1]))&& args[index + 1] != null)
                        {
                            acctquery += " && ";
                        }
                        else if (index < count - 1 && attributes[index] != attributes[index + 1] && !attributes[index + 1].StartsWith("Rfcrff") && args[index + 1] != null)
                        {
                            // add and/or selector for attribute search
                            acctquery += " && ";
                        }
                        else if (index < count - 1 && !attributes[index + 1].StartsWith("Rfcrff") && args[index + 1] != null)
                        {
                            acctquery += " || ";
                        }

                    }
                }

                // adding the searched for account to the accounts processed list
                index++;
            }

            index--;

            //Empty query.
            if (args.Length == 0)
            {
                //TODO: Send an empty list so that the view stops complaining whenever a empty search happens. -jb
                return NotFound();
            }

            ViewBag.ColNames = attributescol;
            ViewBag.ColTypes = attributesTypes;
            ViewBag.ColDisplay = attributesdisplay;

            if (query != null && acctquery != null)
            {
                List<CustomerResult> results = repo.searchByCustomerAndAccountAttributes(query, acctquery);
                return View(results);
            }
            else if (query !=null && acctquery == null)
            {
                List<MRcfCrf> customers = repo.getCustomersByQuery(query);
                List<CustomerResult> results = repo.searchByCustomers(customers);
                return View(results);
            }
            else
            {
                List<CustomerResult> results = repo.searchByAttributeAccount(acctquery);
                return View(results);
            }
        }
    }
}




