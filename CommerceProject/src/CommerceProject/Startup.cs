﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using CommerceProject.Models;
using Microsoft.EntityFrameworkCore;
using Stormpath.AspNetCore;
using Stormpath.Configuration.Abstractions;

namespace CommerceProject
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            if (env.IsDevelopment())
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);

            services.AddStormpath(new StormpathConfiguration()
            {
                Web = new WebConfiguration()
                {
                    ForgotPassword = new WebForgotPasswordRouteConfiguration()
                    {
                        Enabled = false
                    },
                    ChangePassword = new WebChangePasswordRouteConfiguration()
                    {
                        Enabled = false
                    },
                    Login = new WebLoginRouteConfiguration()
                    {
                        Enabled = true
                    },
                    Logout = new WebLogoutRouteConfiguration()
                    {
                        Enabled = true
                    },
                    Me = new WebMeRouteConfiguration()
                    {
                        Enabled = true
                    },
                    Oauth2 = new WebOauth2RouteConfiguration()
                    {
                        Enabled = true
                    },
                    Register = new WebRegisterRouteConfiguration()
                    {
                        Enabled = true
                    },
                    VerifyEmail = new WebVerifyEmailRouteConfiguration()
                    {
                        Enabled = false
                    }
                }
            });

        
        services.AddMvc();

            var connection = @"Server=tcp:bteam.database.windows.net,1433;Initial Catalog=commercedb;Persist Security Info=False;User ID=bteam;Password=umkcCS451R;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            //var connection = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Commerce;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            services.AddDbContext<CommerceDbContext>(options => options.UseSqlServer(connection));
 

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseApplicationInsightsRequestTelemetry();

            

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseApplicationInsightsExceptionTelemetry();

            app.UseStaticFiles();

            app.UseStormpath();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
